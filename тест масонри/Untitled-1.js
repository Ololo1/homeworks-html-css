$(document).ready(function () {

    var sizer = '.sizer4';
    var container = $('#gallery-masonry');

    container.imagesLoaded(function () {
        container.masonry({
            itemSelector: '.item-masonry',
            columnWidth: sizer,
            percentPosition: true,
        });
    });

});